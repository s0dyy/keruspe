# Copyright 2012-2014 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

myexparam gnome_shell_version

exparam -v GS_VER gnome_shell_version

require bash-completion github [ user=Keruspe ] zsh-completion
require freedesktop-desktop gsettings
require vala [ vala_dep=true with_opt=true ]
require meson

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Clipboard management system"
HOMEPAGE="https://www.github.com/Keruspe/${PN}/"

LICENCES="BSD-2"
SLOT="0"
MYOPTIONS="
    bash-completion
    gnome-shell [[ description = [ Install the gnome-shell extension ] requires = gobject-introspection ]]
    gobject-introspection
    vapi [[ requires = gobject-introspection ]]
    zsh-completion
"

MESON_SRC_CONFIGURE_PARAMS=(
    "-Dx-keybinder=true"
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    "bash-completion"
    "gnome-shell"
    "gobject-introspection introspection"
    "vapi"
    "zsh-completion"
)
#    "bash-completion bashcompletiondir ${BASHCOMPLETIONDIR}"
#    "zsh-completion zshcompletiondir ${ZSHCOMPLETIONDIR}"

DEPENDENCIES="
    build:
        dev-libs/appstream-glib
        sys-devel/gettext
        virtual/pkg-config[>=0.27]
    build+run:
        dev-libs/glib:2[>=2.70.0]
        dev-libs/libadwaita:1[>=1.1][gobject-introspection?][vapi?]
        gnome-desktop/gcr:0[>=3.41.0]
        gnome-desktop/gnome-control-center
        sys-apps/dbus
        x11-libs/gdk-pixbuf:2.0[>=2.38.0]
        x11-libs/gtk+:3[>=3.24.0][gobject-introspection?]
        x11-libs/gtk:4.0[>=4.6.0]
        x11-libs/libX11
        x11-libs/libXi
        gnome-shell? (
            gnome-bindings/gjs:1[>=1.54.0]
            gnome-desktop/mutter
            x11-libs/pango
        )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.58.0] )
    run:
        gnome-shell? ( gnome-desktop/gnome-shell[${GS_VER}] )
        zsh-completion? ( app-shells/zsh )
"

GPaste_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
}

GPaste_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
}
